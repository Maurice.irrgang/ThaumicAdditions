#Project info
author=Zeitheron
start_git_commit=74d8304702f241ca09a48c1a10315fdee8272074
git_commit=https://gitlab.com/DragonForge/ThaumicAdditions/commit/{{hash}}

#Runtime info
forge_version=14.23.5.2768
minecraft_version=1.12.2
mcp=stable_39
hc_version=2.0.4.2
jei_version=4.11.0.206

#Mod info
trusted_build=false
mod_id=thaumadds
mod_id_fancy=ThaumicAdditions
mod_name=Thaumic Additions: Reconstructed
mod_version=11r
cf_project=232564
release_type=release